# Welcome to CCPD3V-T3@M #

Our operations teams needs to create a basic API for the management of its users.

### Requeriments? ###

* Laravel 7.x above or Lumen
* Migrations
* Authentification using Token   (recommended: https://jwt-auth.readthedocs.io/en/docs/) 
* Define routes using routes/api (avoid: routes/web)

### Migrations? ###

* create a database named: dev_database_yourname_lastname
* Table 1 : users (id, name, email, password, status, created_at) (recommend: /databases/migrations/create_users...)
* Table 2 : calls (id, called_number, user_id, created_at)
* Table 3 : roles (id, name, user_id) (recommended: https://github.com/spatie/laravel-permission)

### API? ###

* POST: /api/auth/login - BODY: {email,password} - Response {access_token,token_type,expires_in}
* GET : /api/users      - RESPONSE: a list of users: [{id,name,email,role}]
* GET : /api/users/id   - RESPONSE: a single user {id,name,email,role}
* POST: /api/users      - BODY: {name,email,role} - RESPONSE: http code 201 succesfully
* PUT : /api/users/id 
* DELETE: /api/users/id


### Complete? ###

* Clone, Request
* Create a new branch with the following name: [development-firstname-lastname]
* Commit
* Push to your branch
* You can re-write .readme in your branch and explain how to set up your project.
* Enjoy :)

### Note ###

* You do not need to build views (just response an api restful)